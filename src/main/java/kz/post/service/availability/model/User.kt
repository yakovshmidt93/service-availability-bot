package kz.post.service.availability.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "paymentbot_users")
class User(@Id
           val name: String = "",
           @Column(name = "chat_id")
           val chatId: String = "")