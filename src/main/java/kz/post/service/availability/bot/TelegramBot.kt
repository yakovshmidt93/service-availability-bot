package kz.post.service.availability.bot

import kz.post.service.availability.config.BotProperties
import kz.post.service.availability.service.TimerService
import kz.post.service.availability.service.UserService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import org.telegram.telegrambots.api.methods.send.SendMessage
import org.telegram.telegrambots.api.objects.Update
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.exceptions.TelegramApiRequestException


@Component
class TelegramBot : TelegramLongPollingBot() {

    private val log = LoggerFactory.getLogger(TelegramBot::class.java)

    @Autowired
    private lateinit var botProperties: BotProperties
    @Autowired
    private lateinit var userService: UserService
    @Autowired
    private lateinit var timerService: TimerService

    override fun getBotToken() = botProperties.token

    override fun getBotUsername() = botProperties.username

    override fun onUpdateReceived(update: Update?) {
    }

    @Scheduled(cron = "*/5 * * * * *")
    internal fun serviceAvailableSchedule() {
        val unavailableServiceTime = timerService.getUnavailableServiceTime()
        if (unavailableServiceTime > 0.0)
            sendMessageToAllUsers("✅ Connection has restored. Service was unavailable $unavailableServiceTime seconds")
        else if (unavailableServiceTime == -1.0)
            sendMessageToAllUsers("❌ Service broke down. Please, check the service's availability")
    }

    private fun sendMessageToAllUsers(message: String) {
        val chatIds = userService.findAllChatIds()
        for (chatId in chatIds)
            sendMessage(chatId, message)
    }

    private fun sendMessage(chatId: String, text: String) {
        val message = SendMessage()
        message.chatId = chatId
        message.text = text

        try {
            execute(message)
        } catch (e: TelegramApiRequestException) {
            log.error("${e.errorCode} ${e.apiResponse}")
        }

    }
}