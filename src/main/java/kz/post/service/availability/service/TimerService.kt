package kz.post.service.availability.service

interface TimerService {

    fun getUnavailableServiceTime(): Double
}