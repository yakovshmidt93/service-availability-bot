package kz.post.service.availability.service

interface UserService {

    fun findAllChatIds(): List<String>
}
