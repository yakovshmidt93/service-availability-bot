package kz.post.service.availability.service

import kz.post.service.availability.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
open class UserServiceImpl(@Autowired
                           private val userRepository: UserRepository) : UserService {

    override fun findAllChatIds(): List<String> {
        return userRepository.findAllChatIds()
    }
}
