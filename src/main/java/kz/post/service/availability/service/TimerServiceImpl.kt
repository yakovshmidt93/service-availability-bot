package kz.post.service.availability.service

import kz.post.service.availability.util.ConnectionHelper
import org.springframework.stereotype.Service
import org.springframework.util.StopWatch
import java.io.IOException
import java.net.UnknownHostException

@Service
class TimerServiceImpl(private val connectionHelper: ConnectionHelper) : TimerService {

    private val serviceCrashedConditionalNumber = -1.0
    private val serviceStillNotWorkingConditionalNumber = 0.0
    private var serviceUnavailableStopWatch = StopWatch()

    override fun getUnavailableServiceTime(): Double {
        try {
            connectionHelper.checkConnection()
            if (serviceUnavailableStopWatch.isRunning) return stopTimer()
        } catch (e: UnknownHostException) {
            if (!serviceUnavailableStopWatch.isRunning) return startTimer()
        } catch (e: IOException) {
            if (!serviceUnavailableStopWatch.isRunning) return startTimer()
        }
        return serviceStillNotWorkingConditionalNumber
    }

    private fun startTimer(): Double {
        serviceUnavailableStopWatch = StopWatch()
        serviceUnavailableStopWatch.start()
        return serviceCrashedConditionalNumber
    }

    private fun stopTimer(): Double {
        serviceUnavailableStopWatch.stop()
        return serviceUnavailableStopWatch.totalTimeSeconds
    }
}
