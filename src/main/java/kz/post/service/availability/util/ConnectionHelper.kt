package kz.post.service.availability.util

import kz.post.service.availability.config.ServiceProperties
import org.springframework.stereotype.Component
import java.net.Socket

@Component
class ConnectionHelper(private val serviceProperties: ServiceProperties) {

    internal fun checkConnection() {
        Socket(serviceProperties.host, serviceProperties.port.toInt()).close()
    }
}