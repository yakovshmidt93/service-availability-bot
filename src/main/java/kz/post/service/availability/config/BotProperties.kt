package kz.post.service.availability.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "bot")
open class BotProperties {
    lateinit var username: String
    lateinit var token: String
}
