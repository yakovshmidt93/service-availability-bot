package kz.post.service.availability.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "service")
open class ServiceProperties {
    lateinit var host: String
    lateinit var port: String
}
