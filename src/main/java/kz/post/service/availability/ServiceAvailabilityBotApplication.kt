package kz.post.service.availability

import kz.post.service.availability.bot.TelegramBot
import kz.post.service.availability.constant.Constants
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Bean
import org.springframework.core.env.Environment
import org.springframework.core.env.SimpleCommandLinePropertySource
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler
import org.telegram.telegrambots.ApiContextInitializer
import org.telegram.telegrambots.TelegramBotsApi
import java.util.*
import javax.annotation.PostConstruct

@SpringBootApplication
@EnableScheduling
@EntityScan("kz.post.service.availability")
@EnableJpaRepositories("kz.post.service.availability.repository")
@EnableAsync
open class ServiceAvailabilityBotApplication
@Autowired
constructor(private val telegramBot: TelegramBot, private val env: Environment) : CommandLineRunner {

    private val log = LoggerFactory.getLogger(ServiceAvailabilityBotApplication::class.java)

    @PostConstruct
    fun init() {
        val botsApi = TelegramBotsApi()
        botsApi.registerBot(telegramBot)

        if (env.activeProfiles.isEmpty())
            log.warn("No Spring profile configured, running with default configuration")
        else
            log.info("Running with Spring profile(s) : {}", Arrays.toString(env.activeProfiles))
    }

    @Bean
    open fun taskScheduler() = ConcurrentTaskScheduler()

    override fun run(vararg strings: String) {
    }

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            ApiContextInitializer.init()

            val app = SpringApplication(ServiceAvailabilityBotApplication::class.java)
            val source = SimpleCommandLinePropertySource(*args)
            addDefaultProfile(app, source)

            app.run(*args)
        }

        private fun addDefaultProfile(app: SpringApplication, source: SimpleCommandLinePropertySource) {
            if (!source.containsProperty("spring.profiles.active"))
                app.setAdditionalProfiles(Constants.SPRING_PROFILE_DEVELOPMENT)
        }
    }
}