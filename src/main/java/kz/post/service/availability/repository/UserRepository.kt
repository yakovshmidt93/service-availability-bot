package kz.post.service.availability.repository

import kz.post.service.availability.model.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : JpaRepository<User, String> {

    @Query("SELECT u.chatId FROM User u")
    fun findAllChatIds(): List<String>
}
