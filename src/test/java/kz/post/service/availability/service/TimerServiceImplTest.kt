package kz.post.service.availability.service

import kz.post.service.availability.util.ConnectionHelper
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.doNothing
import org.mockito.Mockito.doThrow
import org.mockito.runners.MockitoJUnitRunner
import java.io.IOException
import java.net.UnknownHostException
import kotlin.test.assertEquals

@Ignore
@RunWith(MockitoJUnitRunner::class)
class TimerServiceImplTest {

    @InjectMocks
    private lateinit var timerService: TimerService
    @Mock
    private lateinit var connectionHelper: ConnectionHelper

    @Test(expected = IOException::class)
    fun `should start timer if service is unavailable`() {
        doThrow(IOException()).`when`(connectionHelper.checkConnection())

        val unavailableServiceTime = timerService.getUnavailableServiceTime()

        assertEquals(-1.0, unavailableServiceTime)
    }

    @Test(expected = UnknownHostException::class)
    fun `should start timer if host is incorrect`() {
        doThrow(UnknownHostException()).`when`(connectionHelper.checkConnection())

        val unavailableServiceTime = timerService.getUnavailableServiceTime()

        assertEquals(-1.0, unavailableServiceTime)
    }

    @Test
    fun `should get unavailable service time`() {
        doNothing().`when`(connectionHelper.checkConnection())

        timerService.getUnavailableServiceTime()
    }
}