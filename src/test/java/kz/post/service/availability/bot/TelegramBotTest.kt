package kz.post.service.availability.bot

import kz.post.service.availability.config.BotProperties
import kz.post.service.availability.service.TimerService
import kz.post.service.availability.service.UserService
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.Spy
import org.mockito.runners.MockitoJUnitRunner
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@RunWith(MockitoJUnitRunner::class)
class TelegramBotTest {

    @InjectMocks
    private lateinit var bot: TelegramBot
    @Mock
    private lateinit var timerService: TimerService
    @Mock
    private lateinit var userService: UserService

    companion object {

        @Spy
        private var botProperties: BotProperties = BotProperties()

        @BeforeClass
        @JvmStatic
        fun setUp() {
            botProperties.token = "testToken"
            botProperties.username = "testUsername"
        }
    }

    @Test
    fun `should return bot token`() {
        val actual = bot.botToken

        assertNotNull(actual)
        assertEquals(botProperties.token, actual)
    }

    @Test
    fun `should return bot username`() {
        val actual = bot.botUsername

        assertNotNull(actual)
        assertEquals(botProperties.username, actual)
    }

    @Test
    fun `should do nothing if service is available`() {
        `when`(timerService.getUnavailableServiceTime()).thenReturn(0.0)

        bot.serviceAvailableSchedule()

        verify(userService, never()).findAllChatIds()
    }

    @Test
    fun `should send message if service was unavailable some time`() {
        `when`(userService.findAllChatIds()).thenReturn(ArrayList())
        `when`(timerService.getUnavailableServiceTime()).thenReturn(1.0)

        bot.serviceAvailableSchedule()

        verify(userService, times(1)).findAllChatIds()
    }

    @Test
    fun `should send message if service just fallen down`() {
        `when`(userService.findAllChatIds()).thenReturn(ArrayList())
        `when`(timerService.getUnavailableServiceTime()).thenReturn(1.0)

        bot.serviceAvailableSchedule()

        verify(userService, times(1)).findAllChatIds()
    }
}